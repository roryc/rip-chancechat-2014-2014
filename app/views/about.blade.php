<!DOCTYPE html>
<html>
  <head>
    <title>ChanceChat</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>  
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- CSS Files comes here -->
    <link href="css/bootstrap.css" rel="stylesheet" media="screen">

    <link href="css/style.css" rel="stylesheet" media="screen">
    <link href="css/animate.css" rel="stylesheet" media="screen">
    <link href="css/owl.carousel.css" rel="stylesheet" media="screen">
    <link href="css/owl.theme.css" rel="stylesheet" media="screen">
    <link href="css/nivo-lightbox.css" rel="stylesheet" media="screen">
    <link href="css/nivo_lightbox_themes/default/default.css" rel="stylesheet" media="screen">
    <link href="css/colors/violet.css" rel="stylesheet" media="screen">
    <link href="css/responsive.css" rel="stylesheet" media="screen">

    <script src="js/popcount.js"></script>
    
    <!-- Google fonts -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic" rel="stylesheet" type="text/css">
    
    <!-- Modernizer and IE specyfic files -->  
    <script src="js/modernizr.custom.js"></script>
      
    

    
  </head>
  

  <body>
  <body onload='timer=setInterval("countUP()", 12000 );'>

    <!--###############################-->
    <!--PRELOADER #####################-->
    <!--###############################-->
    
    <div id="preloader">
        <div id="status">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
    </div>


    <!--###############################-->
    <!--HOME ##########################-->
    <!--###############################-->

    <section id="home">
    <div class="container">
        <div class="row">
            <div id="homescreen" class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                 <div id="logo"><a href="#home"><img src="images/CC_LogoFlat.png" alt="logo" height="200px"></a></div>
                 <div id="logo_header"><img src="images/cc.png" alt="logo" height="60"></div>
                 <div id="slogan"><h1>the best way to meet people near you.</h1></div>
                 <a href="#" class="cta1" id="button_more">Learn More</a>
                 <a href="login/fb" class="cta2" id="button_download">Take a Chance</a>
                 <img src="images/homescreen.png" class="img-responsive" id="home_image" alt="img">
            </div> <div class="clearfix"></div>
        </div>
    </div>
    </section>
    
    
    <!--###############################-->
    <!--ABOUT #########################-->
    <!--###############################-->
    
    <section id="about">
        <div class="container">
            
            <div class="row" id="about_intro">
                <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3" >
                    
                    <h2>Connect with new people</h2>
                    <p>shoutz to the nigger government for the numbers.</p>
                    
                </div>
            </div>
            
            <div class="row" >
                
                <div class="col-sm-4 col-md-4 col-lg-4" id="service_1">
                    <svg viewBox="0 0 32 32" class="services_icon"><use xlink:href="#"></use></svg>
                    <h3><div id="timer_container">318902390</div></h3>
                    <p>People Alive in the United States</p>
                </div>
                
                <div class="col-sm-4 col-md-4 col-lg-4" id="service_2">
                    <svg viewBox="0 0 32 32" class="services_icon"><use xlink:href="#"></use></svg>
                    <h3>350</h3>
                    <p>Amount of friends the average Facebook user has</p>
                </div>
                
                <div class="col-sm-4 col-md-4 col-lg-4" id="service_3">
                    <svg viewBox="0 0 32 32" class="services_icon"><use xlink:href="#"></use></svg>
                    <h3>40000000</h3>
                    <p>Amount of people ages 17-25 in the United States</p>
                </div>
                
            </div>
    
        </div>
    </section>
    
    
    <!--Arrows-->
    <section class="arrows_box">
    <div class="arrows">
        <svg viewBox="0 0 32 32" class="arrow_down" id="about_arrow_next">
            <use xlink:href="#right-arrow"></use></svg>
        <svg viewBox="0 0 32 32" class="arrow_up" id="about_arrow_back">
            <use xlink:href="#left-arrow"></use></svg>
    </div>
    </section>
    <!--######-->


    <!--###############################-->
    <!--Features 1 ####################-->
    <!--###############################-->
    
    <!--<section id="features_1">
        <div class="container">
            <div class="row"  >
                <div class="col-sm-6 col-md-6  col-lg-6">
                  <img src="images/features1b.png" class="img-responsive image_back" id="features1a_image" alt="img">
                  <img src="images/features1a.png" class="img-responsive image_front" id="features1b_image" alt="img">
                </div>
                
                <div class="col-sm-6 col-md-6 col-lg-6" id="features_1_content">
                    <h2>Detailed documentation</h2>
                    <p>Epsum factorial non deposit quid pro quo hic escorol. Olypian quarrels et gorilla congolium sic ad nauseum.
                    </p>
                    <div class="feature">
                        <svg viewBox="0 0 32 32" class="feature_icon">
                        <use xlink:href="#graph-pie1"></use></svg>
                        <div>
                            <h4>Beautiful, modern design</h4>
                            <p>Epsum factorial non deposit quid pro quo<br>hic escorol.</p>
                        </div>
                    </div>
                    
                    <div class="feature">
                        <svg viewBox="0 0 32 32" class="feature_icon">
                            <use xlink:href="#joystick"></use></svg>
                        <div>
                            <h4>Easy to set up</h4>
                            <p>Epsum factorial non depositquid pro quo<br>hic escorol.</p>
                        </div>
                    </div>


                </div>
                
            </div>
            
        </div>
       
    </section>

    <!--Arrows-->
    <!--<section class="arrows_box">
        <div class="arrows">
            <svg viewBox="0 0 32 32" class="arrow_down" id="features_1_arrow_next">
                <use xlink:href="#right-arrow"></use></svg>
            <svg viewBox="0 0 32 32" class="arrow_up" id="features_1_arrow_back">
                <use xlink:href="#left-arrow"></use></svg>
        </div>
    </section>
    <!--######-->
    

    <!--###############################-->
    <!--Features 2 ####################-->
    <!--###############################-->

    <!--<section id="features_2">
        <div class="container">
            <div class="row">
                
                <div class="col-sm-6 col-md-6 col-lg-6" id="features_2_content">
                    <h2>All you want from an app</h2>
                    <p>Epsum factorial non deposit quid pro quo hic escorol. Olypian quarrels et gorilla congolium sic ad nauseum.
                    </p>
                    <div class="feature">
                        <svg viewBox="0 0 32 32" class="feature_icon">
                            <use xlink:href="#ribbon"></use></svg>
                        <div>
                            <h4>Video background</h4>
                            <p>Epsum factorial non deposit quid pro quo<br>hic escorol.</p>
                        </div>
                    </div>
                    
                    <div class="feature">
                        <svg viewBox="0 0 32 32" class="feature_icon">
                            <use xlink:href="#rotate"></use></svg>
                        <div>
                            <h4>Cool animations</h4>
                            <p>Epsum factorial non depositquid pro quo<br>hic escorol.</p>
                        </div>
                    </div>
                    
                </div>
                
                
                <div class="col-sm-6 col-md-6  col-lg-6" >
                    <img src="images/features2b.png" class="img-responsive image_back" id="features2a_image" alt="img">
                    <img src="images/features2a.png" class="img-responsive image_front" id="features2b_image" alt="img">
                </div>

            </div>
            
        </div>
        
    </section>


    <!--Arrows-->
    <!--<section class="arrows_box arrows_no_border">
        <div class="arrows">
            <svg viewBox="0 0 32 32" class="arrow_down" id="features_2_arrow_next">
                <use xlink:href="#right-arrow"></use></svg>
            <svg viewBox="0 0 32 32" class="arrow_up" id="features_2_arrow_back">
                <use xlink:href="#left-arrow"></use></svg>
        </div>
    </section>
    <!--######-->


    <!--###############################-->
    <!--Features 3 ####################-->
    <!--###############################-->

    <!--<section id="features_3">
        <div class="container">
            <div class="row" id="features_3_intro">
                <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3" >
                    <h2>Awesome Features</h2>
                    <p>Epsum factorial non deposit quid pro quo hic escorol. Olypian quarrels et gorilla congolium sic ad nauseum.
                    </p>
                </div>
            </div>
            
            <div class="row" >
                <div class="col-sm-4 col-md-4 ol-lg-4" id="features_3_content_left">
                    
                    <div class="feature">
                        <h4>Awesome Features</h4>
                        <p>Epsum factorial non deposit quid pro quo hic escorol. Olypian quarrels et gorilla congolium sic ad nauseum.
                        </p>
                    </div>
                    
                    <div class="feature">
                        <h4>Awesome Features</h4>
                        <p>Epsum factorial non deposit quid pro quo hic escorol. Olypian quarrels et gorilla congolium sic ad nauseum.
                        </p>
                    </div>
                    
                </div>
            
                <div class="col-sm-4 col-md-4 ol-lg-4" id="features_3_content_center">
                    <img src="images/features3.png" class="img-responsive" id="features3_image" alt="img">
                </div>

                <div class="col-sm-4 col-md-4 ol-lg-4" id="features_3_content_right">
                    
                    <div class="feature">
                        <h4>Awesome Features</h4>
                        <p>Epsum factorial non deposit quid pro quo hic escorol. Olypian quarrels et gorilla congolium sic ad nauseum.
                        </p>
                    </div>
                    
                    <div class="feature">
                        <h4>Awesome Features</h4>
                        <p>Epsum factorial non deposit quid pro quo hic escorol. Olypian quarrels et gorilla congolium sic ad nauseum.
                        </p>
                    </div>
                    
                </div>
           
            </div>
            
        </div>
    </section>


    <!--Arrows - version with custom background color to match section above-->
    <section class="arrows_box arrows_box_bg arrows_no_border">
        <div class="arrows">
            <svg viewBox="0 0 32 32" class="arrow_down" id="features_3_arrow_next">
                <use xlink:href="#right-arrow"></use></svg>
            <svg viewBox="0 0 32 32" class="arrow_up" id="features_3_arrow_back">
                <use xlink:href="#left-arrow"></use></svg>
        </div>
    </section>
    <!--######-->


    <!--###############################-->
    <!--Gallery #######################-->
    <!--###############################-->

    <section id="gallery">
        <div class="container">
            <div class="row" id="gallery_intro">
                <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3" >
                    
                    <h2>Screenshots Gallery</h2>
                    <p>pics of girls on cam</p>
                    
                </div>
            </div>
            
            <div class="row" id="gallery_carousel">
                <div class="col-sm-12 col-md-12 col-lg-12" >
                    <div id="owl-gallery" class="owl-carousel">
                        <div class="item"><a href="images/gallery1.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/gallery1@2x.jpg"><img src="images/gallery1.jpg" class="img-responsive img-rounded" alt="img"></a></div>
                        <div class="item"><a href="images/gallery2.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/gallery2@2x.jpg"><img src="images/gallery2.jpg" class="img-responsive img-rounded" alt="img"></a></div>
                        <div class="item"><a href="images/gallery3.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/gallery3@2x.jpg"><img src="images/gallery3.jpg" class="img-responsive img-rounded" alt="img"></a></div>
                        <div class="item"><a href="images/gallery4.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/gallery4@2x.jpg"><img src="images/gallery4.jpg" class="img-responsive img-rounded" alt="img"></a></div>
                        <div class="item"><a href="images/gallery5.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/gallery5@2x.jpg"><img src="images/gallery5.jpg" class="img-responsive img-rounded" alt="img"></a></div>
                        <div class="item"><a href="images/gallery6.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/gallery6@2x.jpg"><img src="images/gallery6.jpg" class="img-responsive img-rounded" alt="img"></a></div>
                        <div class="item"><a href="images/gallery7.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/gallery7@2x.jpg"><img src="images/gallery7.jpg" class="img-responsive img-rounded" alt="img"></a></div>
                        <div class="item"><a href="images/gallery8.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/gallery8@2x.jpg"><img src="images/gallery8.jpg" class="img-responsive img-rounded" alt="img"></a></div>
                        <div class="item"><a href="images/gallery9.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/gallery9@2x.jpg"><img src="images/gallery9.jpg" class="img-responsive img-rounded" alt="img"></a></div>
                        <div class="item"><a href="images/gallery10.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/gallery10@2x.jpg"><img src="images/gallery10.jpg" class="img-responsive img-rounded" alt="img"></a></div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
    
    
    <!--Arrows - single arrow wersion-->
    <section class="arrows_box">
        <div class="arrow_single">
            <svg viewBox="0 0 32 32" class="arrow_up" id="gallery_arrow_back">
                <use xlink:href="#left-arrow"></use></svg>
        </div>
    </section>
    <!--######-->
    
    
    <!--###############################-->
    <!--Newsletter and Footer #########-->
    <!--###############################-->
    
    <section id="footer">
        <div class="container">
            <div class="row" id="newsletter">
                <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3" >
                    
                    <h2>Maybe Facebook like? I really dont know.</h2>
                    <p>spamtec email them they have a nice email script included with the theme.</p>
                    
                    <div id="newsletter_form">
                        <form action="" method="post" class="subscribe-form" id="subscribe-form">
                            <input type="email" name="email" class="subscribe-input" placeholder="Enter your e-mail address..."  required>
                                <button type="submit" class="subscribe-submit">Notify Me</button>
                        </form>
                    </div>
                    <div id="preview"></div>
                    
                </div>
            </div>
           
           <div class="row">
               <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1" id="share">
                   
                   <div id="social_icons">
                       <ul>
                           <li><svg viewBox="0 0 32 32" id="fb_icon" onclick="location.href='#'"><use xlink:href="#facebook"></use></svg></li>
                           <li><svg viewBox="0 0 32 32" id="tw_icon" onclick="location.href='#''"><use xlink:href="#twitter"></use></svg></li>
                           <li><svg viewBox="0 0 32 32" id="in_icon" onclick="location.href='#'"><use xlink:href="#linkedin"></use></svg></li>
                           <li><svg viewBox="0 0 32 32" id="g_icon" onclick="location.href='#'"><use xlink:href="#google-plus"></use></svg></li>
                           <li><svg viewBox="0 0 32 32" id="pin_icon" onclick="location.href='#'"><use xlink:href="#pinterest"></use></svg></li>
                           <li><svg viewBox="0 0 32 32" id="fli_icon" onclick="location.href='#'"><use xlink:href="#flickr"></use></svg></li>
                       </ul>
                   </div>
                   
                   <p>Copyright &copy; 2014 ChanceChat</p>
     
               </div>
           </div>
           
        </div>
    </section>
    


    <!-- JavaScript plugins comes here -->
    <script src="js/jquery-2.0.3.min.js"></script>

    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.scrollTo.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/main.js"></script>
    <script src="js/retina.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/nivo-lightbox.min.js"></script>
    <script type="text/javascript">
        $('document').ready(function(){
                $('#subscribe-form').ajaxForm( {
                target: '#preview',
                success: function() { 
                      $('#subscribe-form').slideUp('slow');
                      $('#preview').css({"opacity":"1"});
                    }
                });
            });
    </script>
    <script>
        var url ='images/icons.svg';
        var c=new XMLHttpRequest(); c.open('GET', url, false); c.setRequestHeader('Content-Type', 'text/xml'); c.send();
        document.body.insertBefore(c.responseXML.firstChild, document.body.firstChild)
    </script>

    

    
  </body>
</html>