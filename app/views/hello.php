<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel PHP Framework</title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			color: #999;
		}

		.welcome {
			width: 300px;
			height: 200px;
			position: absolute;
			left: 50%;
			top: 50%;
			margin-left: -150px;
			margin-top: -100px;
		}

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}
	</style>
</head>
<body>
<?php

$application = array(
    'appId' => '345296832288344',
    'secret' => '4e6ed6f9cd5a6644f434949182170a68'
    );
$permissions = 'publish_stream';
$url_app = 'http://104.131.255.181/';

// getInstance
FacebookConnect::getFacebook($application);

$getUser = FacebookConnect::getUser($permissions, $url_app); // Return facebook User data

echo "<pre>";
var_dump($getUser);
echo "</pre>";
?>
</body>
</html>
