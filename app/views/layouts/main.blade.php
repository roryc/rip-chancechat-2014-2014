<?php
$facebook_albums = new Facebook(Config::get('facebook'));

$album = $facebook_albums->api('/me');

?>
<html>
   
        <head>
            {{ HTML::script('js/vendors/modernizr/modernizr.custom.js'); }}
            {{ HTML::style('css/styles.css'); }}
            {{ HTML::style('css/styles-blue.css'); }}
            <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
            <script src="http://connect.facebook.net/en_US/all.js"></script>
            <script>
              FB.init({
              appId:'655850271142591',
              redirect_uri: 'http://www.google.com?id=323232',
              data: '323232',
              cookie:true,
              status:true,
              xfbml:true
              });

              function FacebookInviteFriends()
              {
              FB.ui({
              method: 'apprequests',
              message: 'Your Message diaolog'
              });
              }
            </script>
        </head>
    
<body>
<!--Smooth Scroll-->
<div class="smooth-overflow">
<!--Navigation-->
    <nav class="main-header clearfix" role="navigation"> 
        <a class="navbar-brand" href="/">
            <span class="text-blue">ChanceChat</span>
        </a> 
      
      <!--Search-->
     <!-- <div class="site-search"> -->
       <!-- <form action="#" id="inline-search"> -->
         <!-- <i class="fa fa-search"></i> -->
          <!-- <input type="search" placeholder="Search"> -->
        <!-- </form> -->
      <!-- </div> -->
      
      <!--Navigation Itself-->
      @section('navigation')
      <div class="navbar-content"> 
        
        <!--Right Userbar Toggler--> 
        <a href="#" class="btn btn-user right-toggler pull-right"> 
            <i class="entypo-vcard"></i>
            <span class="logged-as hidden-xs">Logged as</span>
            <span class="logged-as-name hidden-xs">Anton Durant</span></a> 
        
        <!--Notifications Dropdown-->
        
        <!--<div class="btn-group">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> 
          <i class="entypo-megaphone"></i></button>
          <div id="notification-dropdown" class="dropdown-menu">
            <div class="dropdown-header">Notifications <span class="badge pull-right">8</span></div>
            <div class="dropdown-container">
              <div class="nano">
                <div class="nano-content">
                  <ul class="notification-dropdown">
                    <li class="bg-warning">
                    <a href="#"> 
                    <span class="notification-icon"><i class="fa fa-bolt"></i></span>
                      <h4>Server Down</h4>
                      <p>Server #435 was shut down (Power loss)</p>
                      <span class="label label-default"><i class="entypo-clock"></i> 59 mins ago</span> </a> </li>
                    <li class="bg-info"><a href="#"> <span class="notification-icon"><i class="fa fa-bolt"></i></span>
                      <h4>Too Many Connections</h4>
                      <p>Too many connections to Database Server</p>
                      <span class="label label-default"><i class="entypo-clock"></i> 2 hours ago</span> </a> </li>
                    <li class="bg-danger"><a href="#"> <span class="notification-icon"><i class="fa fa-android"></i></span>
                      <h4>Sausage Stolen</h4>
                      <p>Someone stole your hot sausage</p>
                      <span class="label label-default"><i class="entypo-clock"></i> 3 hours ago</span> </a> </li>
                    <li class="bg-success"><a href="#"> <span class="notification-icon"><i class="fa fa-bolt"></i></span>
                      <h4>Defragmentation Completed</h4>
                      <p>Disc Defragmentation Completed on Server</p>
                      <span class="label label-default"><i class="entypo-clock"></i> 3 hours ago</span> </a> </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="dropdown-footer"><a class="btn btn-dark" href="#">See All</a></div>
          </div>
        </div>-->
        
        <!--Inbox Dropdown-->
        <!--<div class="btn-group">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="entypo-mail"></i></button>
          <div id="inbox-dropdown" class="dropdown-menu inbox">
            <div class="dropdown-header">Inbox <span class="badge pull-right">32</span></div>
            <div class="dropdown-container">
              <div class="nano">
                <div class="nano-content">
                  <ul class="inbox-dropdown">
                    <li><a href="#"> <span class="user-image"><img src="http://placehold.it/150x150" alt="Gluck Dorris" /></span>
                      <h4>Why don't u answer calls?</h4>
                      <p>Listen, dude, time is off. I'll find you soon! Sounds good?...</p>
                      <span class="label label-default"><i class="entypo-clock"></i> 59 mins ago</span> <span class="delete"><i class="entypo-back"></i></span> </a> </li>
                    <li><a href="#"> <span class="user-image"><img src="http://placehold.it/150x150" alt="Gluck Dorris" /></span>
                      <h4>Rawrr, rawrrr...</h4>
                      <p>Listen, dude, time is off. I'll find you soon! Sounds good?...</p>
                      <span class="label label-default"><i class="entypo-clock"></i> 2 hours ago</span> <span class="delete"><i class="entypo-back"></i></span> </a> </li>
                    <li><a href="#"> <span class="user-image"><img src="http://placehold.it/150x150" alt="Gluck Dorris" /></span>
                      <h4>Why so serious?</h4>
                      <p>Listen, dude, time is off. I'll find you soon! Sounds good?...</p>
                      <span class="label label-default"><i class="entypo-clock"></i> 3 hours ago</span> <span class="delete"><i class="entypo-back"></i></span> </a> </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="dropdown-footer"><a class="btn btn-dark" href="admin-inbox.html">Save All</a></div>
          </div>
        </div>
      </div>
    </nav>-->
    
    <!--/Navigation--> 
    
    <!--MainWrapper-->
    <div class="main-wrap"> 
      
      <!-- /Offcanvas user menu-->
      <aside class="user-menu"> 
        
        <!-- Tabs -->
        <div class="tabs-offcanvas">
          <div class="tab-content"> 
            
            <!--User Primary Panel-->
            <div class="tab-pane active" id="userbar-one">
              <div class="main-info">
                <div class="user-img">
                <img src="<?php echo $data['photo']; ?>" alt="User Picture" width="150" /></div>
                <h1>Hi, <?php echo $album['first_name']; ?>!</h1>
              </div>
              <div class="list-group"> <a href="#" class="list-group-item"><i class="fa fa-user"></i>Profile</a> <a href="#" class="list-group-item"><i class="fa fa-cog"></i>Settings</a> <a href="#" class="list-group-item"><i class="fa fa-flask"></i>Projects<span class="badge">2</span></a>
                <div class="empthy"></div>
                <a href="#" class="list-group-item"><i class="fa fa-refresh"></i>Updates<span class="badge">5</span></a> <a href="#" class="list-group-item"><i class="fa fa-comment"></i>Messages<span class="badge">12</span></a> <a href="#" class="list-group-item"><i class="fa fa-comments"></i> Comments<span class="badge">45</span></a>
                <div class="empthy"></div>
                <a href="#" data-toggle="modal" class="list-group-item lockme"><i class="fa fa-lock"></i> Lock</a> 
                <a href="logout" class="list-group-item goaway"><i class="fa fa-power-off"></i> Sign Out</a> </div>
            </div>
            
         
          </div>
        </div>
        
        <!-- /tabs --> 
        
      </aside>
      <!-- /Offcanvas user menu--> 
      
     
      <!--Content Wrapper-->
      <div class="content-wrapper"> 
        
        <!--Horisontal Dropdown-->
        <!--<nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
          <div class="cbp-hsinner">
            <ul class="cbp-hsmenu">
              <li> <a href="#"><span class="icon-bar"></span></a>
                <ul class="cbp-hssubmenu">
                  <li><a href="#">
                    <div class="sparkle-dropdown"><span class="inlinebar">10,8,8,7,8,9,7,8,10,9,7,5</span>
                      <p class="sparkle-name">project income</p>
                      <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                    </div>
                    </a></li>
                  <li><a href="#">
                    <div class="sparkle-dropdown"><span class="linechart">5,6,7,9,9,5,3,2,9,4,6,7</span>
                      <p class="sparkle-name">site traffic</p>
                      <p class="sparkle-amount">122541 <i class="fa fa-chevron-circle-down"></i></p>
                    </div>
                    </a></li>
                  <li><a href="#">
                    <div class="sparkle-dropdown"><span class="simpleline">9,6,7,9,3,5,7,2,1,8,6,7</span>
                      <p class="sparkle-name">Processes</p>
                      <p class="sparkle-amount">890 <i class="fa fa-plus-circle"></i></p>
                    </div>
                    </a></li>
                  <li><a href="#">
                    <div class="sparkle-dropdown"><span class="inlinebar">10,8,8,7,8,9,7,8,10,9,7,5</span>
                      <p class="sparkle-name">orders</p>
                      <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                    </div>
                    </a></li>
                  <li><a href="#">
                    <div class="sparkle-dropdown"><span class="piechart">1,2,3</span>
                      <p class="sparkle-name">active/new</p>
                      <p class="sparkle-amount">500/200 <i class="fa fa-chevron-circle-up"></i></p>
                    </div>
                    </a></li>
                  <li><a href="#">
                    <div class="sparkle-dropdown"><span class="stackedbar">3:6,2:8,8:4,5:8,3:6,9:4,8:1,5:7,4:8,9:5,3:5</span>
                      <p class="sparkle-name">fault/success</p>
                      <p class="sparkle-amount">$23989 <i class="fa fa-chevron-circle-up"></i></p>
                    </div>
                    </a></li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>-->
        
       <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
          <div class="col-md-12 bootstrap-grid"> 
            
            <!-- New widget -->
            <div class="powerwidget blue" id="startwidget">
              
              <header>
                <h2>Take a Chance</h2>
              </header>
              
              <div>
                    <div class="inner-spacer">
                    @yield('content')
                    </div>
              </div>
            </div>
            <!-- End Widget --> 
            
          </div>
          <!-- /Inner Row Col-md-12 --> 
        </div>
        <!-- /Widgets Row End Grid--> 
      </div>
      <!-- / Content Wrapper --> 
    </div>
    <!--/MainWrapper--> 
  </div>
<!--/Smooth Scroll--> 


<!-- scroll top -->
<div class="scroll-top-wrapper hidden-xs">
    <i class="fa fa-angle-up"></i>
</div>
<!-- /scroll top -->



<!--Modals--> 
<!--Panel Question Modal-->
<div class="modal fade" id="panel-question">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <i class="fa fa-question"></i> </div>
      <div class="modal-body text-center">How Do you Do?</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<!--Power Widgets Modal-->
<div class="modal" id="delete-widget">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <i class="fa fa-lock"></i> </div>
      <div class="modal-body text-center">
        <p>Are you sure to delete this widget?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="trigger-deletewidget-reset">Cancel</button>
        <button type="button" class="btn btn-primary" id="trigger-deletewidget">Delete</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<!--Scripts--> 
    <!--JQuery-->
    {{ HTML::script('js/vendors/jquery/jquery.min.js'); }} 
    {{ HTML::script('js/vendors/jquery/jquery-ui.min.js'); }} 

    <!--EasyPieChart--> 
    {{ HTML::script('js/vendors/easing/jquery.easing.1.3.min.js'); }} 
    {{ HTML::script('js/vendors/easypie/jquery.easypiechart.min.js'); }}

    <!--Fullscreen--> 
    {{ HTML::script('js/vendors/fullscreen/screenfull.min.js'); }}

    <!--Forms--> 
    {{ HTML::script('js/vendors/forms/jquery.form.min.js'); }}
    {{ HTML::script('js/vendors/forms/jquery.validate.min.js'); }}
    {{ HTML::script('js/vendors/forms/jquery.maskedinput.min.js'); }}
    {{ HTML::script('js/vendors/jquery-steps/jquery.steps.min.js'); }}

    <!--NanoScroller--> 
    {{ HTML::script('js/vendors/nanoscroller/jquery.nanoscroller.min.js'); }}

    <!--Sparkline--> 
    {{ HTML::script('js/vendors/sparkline/jquery.sparkline.min.js'); }}

    <!--Horizontal Dropdown--> 
    {{ HTML::script('js/vendors/horisontal/cbpHorizontalSlideOutMenu.js'); }}
    {{ HTML::script('js/vendors/classie/classie.js'); }}

    <!--Datatables--> 
    {{ HTML::script('js/vendors/datatables/jquery.dataTables.min.js'); }}
    {{ HTML::script('js/vendors/datatables/jquery.dataTables-bootstrap.js'); }}
    {{ HTML::script('js/vendors/datatables/dataTables.colVis.js'); }}
    {{ HTML::script('js/vendors/datatables/colvis.extras.js'); }}

    <!--PowerWidgets--> 
    {{ HTML::script('js/vendors/powerwidgets/powerwidgets.js'); }}

    <!--Summernote--> 
    {{ HTML::script('js/vendors/summernote/summernote.min.js'); }}

    <!--Bootstrap--> 
    {{ HTML::script('js/vendors/bootstrap/bootstrap.min.js'); }}

    <!--Bootstrap Progress Bar--> 
    {{ HTML::script('js/vendors/bootstrap-progress-bar/bootstrap-progressbar.min.js'); }} 

    <!--Main App--> 
    {{ HTML::script('js/scripts.js'); }} 

<!--/Scripts-->

</body>
    
    </html>
