<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnectionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('connection', function($table)
        {
		$table->increments('id');
		$table->integer('user_id');
		$table->string('connection_code');
		$table->integer('waiting')->default(0);
		$table->integer('active')->default(0);
		$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('connection');
	}

}
