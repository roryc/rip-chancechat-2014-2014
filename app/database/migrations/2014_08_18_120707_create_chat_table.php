<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('live_chats', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('receiver_id')->unsigned();
            $table->string('connection_code');
			$table->text('message');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('live_chats');
    }

}
