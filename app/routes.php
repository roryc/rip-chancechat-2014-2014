<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * What happens at 'localhost'?.
 */
Route::get('/', function()
{
    $data = array();

    if (Auth::check()) {
        $data = Auth::user();
        return View::make('user', array('data'=>$data));
    }
    else{

    	return View::make('about');
    }
    
});


Route::get('chat', function() {
    if (Auth::check()) {
        DB::table('connection')
           ->where('user_id', Auth::user()->id)
           ->update(array('active' => 0));

        $data = Auth::user();
        $user_id = Auth::user()->id;

        $connection_code = md5(Session::getId() . $user_id);

        $date = new \DateTime;

        $users = DB::table('connection')
                    ->where('user_id', $user_id)
                    ->groupBy('user_id')
                    ->having('user_id', '>', 0)
                    ->get();

        if(empty($users)){
        
        DB::table('connection')->insert(
            array('connection_code' => $connection_code, 
                  'user_id' => $user_id,
                  'created_at' => $date,
                  'updated_at' => $date
                  )
        );
        }
        else{
            DB::table('connection')
                    ->where('user_id', Auth::user()->id)
                    ->update(array('connection_code' => $connection_code));
        }

        return View::make('video', array('data'=>$data));

    } else {
        return View::make('about');
    }

});

/**
 * Cron route and functions.
 */

Route::get('logout', function() {
    Auth::logout();
    return Redirect::to('/');
});

/**
 * Logout route function.
 */

Route::get('crons/{action}', function($action) {
    
    if($action == "remove_kebab"){

        while(1){

        $date = date("Y-m-d H:i:s");
        $time = strtotime($date);
        $time = $time - (15);
        $date = date("Y-m-d H:i:s", $time);
        
        DB::delete("DELETE FROM connection WHERE created_at < '".$date."'");

        sleep(15);
        }
    } 

});

Route::get('update/{id}', function($id) {
    
    $date = new \DateTime;
    DB::update('UPDATE connection SET created_at = ? WHERE user_id = ?', array($date, $id));

});

/**
 * Test about.
 */

Route::get('about', function() {
    return View::make('about');
});

/**
 * Facebook login route.
 */

Route::get('login/fb', function() {
    $facebook = new Facebook(Config::get('facebook'));
    $params = array(
        'redirect_uri' => url('/login/fb/callback'),
        'scope' => 'email',
    );
    return Redirect::away($facebook->getLoginUrl($params));
});

/**
 * Creating a route to receive the callback and henceforth define our actions.
 */

Route::get('login/fb/callback', function() {
    
    $code = Input::get('code');
    
    if (strlen($code) == 0) return Redirect::to('/')->with('message', 'There was an error communicating with Facebook');

    $facebook = new Facebook(Config::get('facebook'));
    
    $uid = $facebook->getUser();

    	if ($uid == 0) return Redirect::to('/')->with('message', 'There was an error');

    	$me = $facebook->api('/me');

    	$profile = Profile::whereUid($uid)->first();
    
    		if (empty($profile)) {

		        $user = new User;
		        $user->email = $me['email'];
                $user->facebook_id = $me['id'];
		        $user->photo = 'https://graph.facebook.com/'.$me['id'].'/picture?type=large';

		        $user->save();

		        $profile = new Profile();
		        
		        $profile->uid = $uid;
		        $profile->username = $me['id'];
		        $profile->first_name = $me['first_name'];
		        $profile->last_name = $me['last_name'];
		        
		        $profile = $user->profiles()->save($profile);
   			 }

    			$profile->access_token = $facebook->getAccessToken();
    			$profile->save();

    			$user = $profile->user;

    Auth::login($user);

    return Redirect::to('/')->with('message', 'Logged in with Facebook');
});
